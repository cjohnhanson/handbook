---
layout: page
title: "WordPress Management"
weight: 2
---

We use WordPress to manage our marketing site and blog. This section contains information on how to manage various parts of the site.

## Add Someone to the Team Section

The team section is currently hosted on the [About](https://meltano.com/about/) page.

**Double check that the teammate wants to appear on this page and is okay with their location being posted.**

1. Collect a headshot, location, and social media links from the teammate.
1. Navigate to the [WordPress Admin Console](https://meltano.com/wp-admin/) and select "Team" from the left-side menu. Select "Add Team" from the menu that pops up.
1. Fill out required information: First and Last Name, Position (job title), Location (City/State/Country)
1. Ignore the "Bio" section for now. It's not displayed anywhere.
1. If you have social links you can add them at the bottom. These are nice to have byt not required
1. Click "Publish" on the right-side menu.

## Remove Someone from Team Section

1. 1. Navigate to the [WordPress Admin Console](https://meltano.com/wp-admin/) and select "Team" from the left-side menu.
1. Hover over the profile you'd like to delete and click "Trash". This will move the profile to the [Trash section](https://meltano.com/wp-admin/edit.php?post_status=trash&post_type=team) where it can be restored if needed.
