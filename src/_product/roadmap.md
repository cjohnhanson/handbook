---
layout: page
title: Roadmap
weight: 2
---

Meltano is developed completely in the open on GitLab: <https://gitlab.com/meltano/meltano>. Our [issue tracker](https://gitlab.com/groups/meltano/-/issues), [epics](https://gitlab.com/groups/meltano/-/epics), and [weekly milestones](https://gitlab.com/groups/meltano/-/milestones) can be found there as well.

To get an idea of what the team and community are currently working on, check out the current [iteration](/company/using-gitlab#iterations)'s [Development Flow board](https://gitlab.com/groups/meltano/-/boards/536761?scope=all&utf8=%E2%9C%93&milestone_title=%23upcoming).

If you'd like to look further into the future, the [Milestones board](https://gitlab.com/groups/meltano/-/boards/536761?not[label_name][]=valuestream::Business+Operation&not[label_name][]=kind::Non-Product&iteration_id=Current) has a column for each upcoming milestone.

Be aware that issue [milestones](/company/using-gitlab#milestones) serve more as a rough indication of relative priority than as hard deadlines,
since short-term priorities can change quickly in response to community feedback, and it's hard to predict how much progress can be made in a week.

These efforts are the foundation of our larger vision to enable a full DataOps OS.
To learn more about our strategy around the DataOps OS, check out [this blog post](https://meltano.com/blog/our-strategy-to-achieving-meltanos-ambitious-mission-and-vision/).

### Our Roadmap

In an effort to keep the roadmap relevant and up to date with the current state of issues, we're experimenting with using a `Roadmap` label on issues and linking to a specific board.
The goal of the roadmap board is to be a "near-sighted" roadmap where the current and next month are better defined while the next quarters are more flexible.

[Roadmap Board](https://gitlab.com/groups/meltano/-/boards/3860358?label_name[]=Roadmap)

As of 2022-Q1, our [DataOps OS blog post](https://meltano.com/blog/our-next-steps-for-building-the-dataops-os/) is a valid summary of what we're building over the next few months. 

As this projects out into the future it is subject to change based on feedback.
Don't see something you want on the roadmap? [Make an issue](https://gitlab.com/meltano/meltano/-/issues) and let us know! Miss the previous version of the roadmap? Reach out to Taylor on Slack.
